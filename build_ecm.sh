#!/bin/bash

git clone https://invent.kde.org/frameworks/extra-cmake-modules

cd extra-cmake-modules
mkdir build
cd build

cmake .. -DCMAKE_INSTALL_PREFIX=/opt/win64 -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=/opt/win64/x86_64-mingw-w64-toolchain.cmake -DBUILD_TESTS=OFF
make
make install

cd ../..
rm -rf extra-cmake-modules
