#!/bin/bash

wget https://github.com/steveire/grantlee/archive/master.zip
unzip master.zip
rm master.zip

cd grantlee-master
mkdir build
cd build

cmake .. -DCMAKE_INSTALL_PREFIX=/opt/win64 -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=/opt/win64/x86_64-mingw-w64-toolchain.cmake -DBUILD_TESTS=OFF
make
make install

cd ../..
rm -rf grantlee-master
