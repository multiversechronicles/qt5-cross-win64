FROM debian:bookworm-slim

ARG QT_VERSION=5.15.2

WORKDIR /build

RUN apt-get -yqq update \
 && apt-get -yqq --no-install-recommends install \
        mingw-w64 \
        python3 \
        cmake \
        wget \
        ca-certificates \
        unzip \
        xz-utils \
        build-essential \
        git \
 && apt-get clean \
 && rm -r /var/lib/apt/lists/*

RUN update-alternatives --set x86_64-w64-mingw32-g++ /usr/bin/x86_64-w64-mingw32-g++-posix \
 && update-alternatives --set x86_64-w64-mingw32-gcc /usr/bin/x86_64-w64-mingw32-gcc-posix

COPY x86_64-mingw-w64-toolchain.cmake /opt/win64/
COPY build_*.sh /build/

RUN /bin/bash /build/build_qt.sh $QT_VERSION \
 && /bin/bash /build/build_ecm.sh \
 && /bin/bash /build/build_grantlee.sh \
 && /bin/bash /build/build_yaml-cpp.sh

CMD ["/bin/bash"]
