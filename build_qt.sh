#!/bin/bash

basedir=$(dirname "$(readlink -f "$0")")
log_file="/build/build.log"

prefix="/opt/win64"

if [ "$#" -ne 1 ]; then
    echo "Usage: $0 [qt version]"
    exit 1
fi

qt_version="$1"
qt_version_parts=(${qt_version//./ })
qt_maj_minor="${qt_version_parts[0]}.${qt_version_parts[1]}"

qt_base_name="qt"
qt_source_dir="$qt_base_name-$qt_version"

qt_modules=(
    "qtbase"
    "qtdeclarative"
    "qtsvg"
    "qtimageformats"
    "qtgraphicaleffects"
    "qtquickcontrols2"
)

if [ ! -d "$qt_source_dir" ]; then
    git clone https://invent.kde.org/qt/qt/qt5 -b kde/5.15 $qt_source_dir
    pushd "$qt_source_dir"

    # Only build the specified modules, anything else is unused and will only
    # bloat the image.
    modules=${qt_modules[@]}
    ./init-repository --module-subset="${modules// /,}"
else
    pushd "$qt_source_dir"
fi

echo "Configuring Qt..."
./configure \
    -prefix $prefix \
    -opensource \
    -confirm-license \
    -release \
    -xplatform win32-g++ \
    -device-option CROSS_COMPILE=x86_64-w64-mingw32- \
    -opengl desktop \
    -nomake tests \
    -nomake examples \
    -no-feature-ftp \
    -no-feature-pdf \
    -no-feature-printer \
    -no-feature-d3d12 \
    -no-dbus \
    > $log_file

echo "Building Qt..."
make -j2 >> $log_file
make install >> $log_file

popd
rm -r "$qt_source_dir"
